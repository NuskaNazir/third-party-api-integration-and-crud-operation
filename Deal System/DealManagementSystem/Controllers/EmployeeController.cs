using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;


using DealManagementSystem.Models;
using DealManagementSystem.Services;

namespace DealManagementSystem.Controllers;

[ApiController]
[Route("api/[controller]")]
public class EmployeeController : ControllerBase
{
    private readonly EmployeeService _employeeService;

    public EmployeeController(EmployeeService employeeService)
    {
        _employeeService = employeeService;
    }

    [HttpGet]
    public async Task<ActionResult<List<Employee>>> GetEmployees()
    {
        List<Employee> employees = await _employeeService.GetAll();
        return employees;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<Employee>> GetEmployee(int id)
    {
        var employee = await _employeeService.GetById(id);

        if (employee == null)
        {
            return NotFound();
        }

        return employee;
    } 

    [HttpPost]
    public async Task<ActionResult<Employee>> CreateEmployee(Employee employee)
    {
        _employeeService.Employee.Add(employee);
        await _employeeService.SaveChangesAsync();

        return CreatedAtAction(nameof(GetEmployee), new { id = employee.Id }, employee);
    }


    [HttpPut("{id}")]
    public async Task<IActionResult> UpdateEmployee(int id, Employee employee)
    {
        if (id != employee.Id)
        {
            return BadRequest();
        }

        _employeeService.Entry(Employee).State = EntityState.Modified;

        try
        {
            await _employeeService.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!_employeeService.Any(e => e.Id == id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return NoContent();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteEmployee(int id)
    {
        var employee = await _employeeService.GetEmployee(id);
        if (employee == null)
        {
            return NotFound();
        }

        _employeeService.Remove(employee);
        await _employeeService.SaveChangesAsync();

        return NoContent();
    }
}