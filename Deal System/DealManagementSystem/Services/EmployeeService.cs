using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using DealManagementSystem.Models;

namespace DealManagementSystem.Services;

public class EmployeeService
{
    static readonly HttpClient client = new HttpClient();

    static async Task Main()
    {
        try
        {
            using HttpResponseMessage response = await client.GetAsync("https://6603f95c2393662c31d0463e.mockapi.io/api/employee");
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseBody);
        }
        catch (HttpRequestException e)
        {
            Console.WriteLine("\nException Caught!");
            Console.WriteLine("Message :{0} ", e.Message);
        }
    }
}